package com.atguigu.mmmall.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmmallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(MmmallWareApplication.class, args);
    }

}
