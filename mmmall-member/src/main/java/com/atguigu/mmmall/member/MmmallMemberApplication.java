package com.atguigu.mmmall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmmallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(MmmallMemberApplication.class, args);
    }

}
