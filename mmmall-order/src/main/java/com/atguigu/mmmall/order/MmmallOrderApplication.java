package com.atguigu.mmmall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmmallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(MmmallOrderApplication.class, args);
    }

}
