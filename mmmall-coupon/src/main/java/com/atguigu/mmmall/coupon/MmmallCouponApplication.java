package com.atguigu.mmmall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmmallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(MmmallCouponApplication.class, args);
    }

}
